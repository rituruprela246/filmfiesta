import React from 'react'

export const Footer = () => {
  return (
    

<footer class="fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-gray-200 shadow p-6  dark:bg-gray-900 dark:border-slate-50">
      <p class="text-sm text-center text-gray-500 sm:text-center dark:text-gray-400">© {new Date().getFullYear()}
      {/* {TODO: CHANGE URL ONCE HOSTED} */}
      <a href="https://flowbite.com/" class="hover:text-primary-800">FlimFiesta&trade;</a>. All Rights Reserved.
    </p>
</footer>

  )
}
