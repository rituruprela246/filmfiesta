import { useEffect } from 'react'

export const UseDynamicTitle = (title,defaultTitle='FilmFiesta') => {
  useEffect(() => {
    document.title = title;

    return() => {
        document.title = defaultTitle;
    }
  },[title,defaultTitle]);
}
